<?php

namespace spec\App\Manager;

use App\Entity\MessageInterface;
use App\Manager\MessageManager;
use App\Saver\MessageSaverInterface;
use PhpSpec\ObjectBehavior;

class MessageManagerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(MessageManager::class);
    }

    function let(MessageSaverInterface $saver)
    {
        $this->beConstructedWith($saver);
    }

    function it_marks_message_as_read(MessageSaverInterface $saver, MessageInterface $message)
    {
        $message->setRead()->shouldBeCalled();
        $saver->save($message)->shouldBeCalled();
        $this->beConstructedWith($saver);

        $this->markAsRead($message);
    }

    function it_archives_message(MessageSaverInterface $saver, MessageInterface $message)
    {
        $message->setArchived()->shouldBeCalled();
        $saver->save($message)->shouldBeCalled();
        $this->beConstructedWith($saver);

        $this->archive($message);
    }
}
