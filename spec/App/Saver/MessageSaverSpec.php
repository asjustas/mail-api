<?php

namespace spec\App\Saver;

use App\Entity\MessageInterface;
use App\Saver\MessageSaver;
use Doctrine\ORM\EntityManagerInterface;
use PhpSpec\ObjectBehavior;

class MessageSaverSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(MessageSaver::class);
    }

    function let(EntityManagerInterface $em)
    {
        $this->beConstructedWith($em);
    }

    function it_should_save_message(EntityManagerInterface $em, MessageInterface $message)
    {
        $em->persist($message)->shouldBeCalled();
        $em->flush()->shouldBeCalled();

        $this->beConstructedWith($em);

        $this->save($message);
    }
}
