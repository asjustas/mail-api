<?php

namespace spec\App\Importer;

use App\Importer\MessagesImporter;
use App\Saver\MessageSaverInterface;
use PhpSpec\ObjectBehavior;

class MessagesImporterSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(MessagesImporter::class);
    }

    function let(MessageSaverInterface $saver)
    {
        $this->beConstructedWith($saver);
    }
}
