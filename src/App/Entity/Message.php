<?php

namespace App\Entity;

class Message implements MessageInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $sender;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $message;

    /**
     * @var int
     */
    private $timeSent;

    /**
     * @var bool
     */
    private $read;

    /**
     * @var bool
     */
    private $archived;

    /**
     * Message constructor.
     * @param string $sender
     * @param string $subject
     * @param string $message
     * @param int $timeSent
     * @param bool $read
     * @param bool $archived
     */
    public function __construct(
        string $sender,
        string $subject,
        string $message,
        int $timeSent,
        bool $read,
        bool $archived
    ) {
        $this->sender = $sender;
        $this->subject = $subject;
        $this->message = $message;
        $this->timeSent = $timeSent;
        $this->read = $read;
        $this->archived = $archived;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSender(): string
    {
        return $this->sender;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return int
     */
    public function getTimeSent(): int
    {
        return $this->timeSent;
    }

    /**
     * @return bool
     */
    public function isRead(): bool
    {
        return $this->read;
    }

    /**
     * @return bool
     */
    public function isArchived(): bool
    {
        return $this->archived;
    }

    public function setRead()
    {
        $this->read = true;
    }

    public function setArchived()
    {
        $this->archived = true;
    }

    /**
     * {@inheritdoc}
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'sender' => $this->sender,
            'subject' => $this->subject,
            'message' => $this->message,
            'time_sent' => $this->timeSent,
            'read' => $this->read,
            'archived' => $this->archived,
        ];
    }
}
