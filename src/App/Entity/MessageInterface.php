<?php

namespace App\Entity;

use JsonSerializable;

interface MessageInterface extends JsonSerializable
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getSender(): string;

    /**
     * @return string
     */
    public function getSubject(): string;

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @return int
     */
    public function getTimeSent(): int;

    /**
     * @return bool
     */
    public function isRead(): bool;

    /**
     * @return bool
     */
    public function isArchived(): bool;

    public function setRead();

    public function setArchived();
}
