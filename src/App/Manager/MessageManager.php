<?php

namespace App\Manager;

use App\Entity\MessageInterface;
use App\Saver\MessageSaverInterface;

class MessageManager implements MessageManagerInterface
{
    /**
     * @var MessageSaverInterface
     */
    private $saver;

    /**
     * MessageManager constructor.
     * @param MessageSaverInterface $saver
     */
    public function __construct(MessageSaverInterface $saver)
    {
        $this->saver = $saver;
    }

    /**
     * @param MessageInterface $message
     */
    public function markAsRead(MessageInterface $message)
    {
        $message->setRead();

        $this->saver->save($message);
    }

    /**
     * @param MessageInterface $message
     */
    public function archive(MessageInterface $message)
    {
        $message->setArchived();

        $this->saver->save($message);
    }
}
