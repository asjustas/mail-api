<?php

namespace App\Manager;

use App\Entity\MessageInterface;

interface MessageManagerInterface
{
    /**
     * @param MessageInterface $message
     */
    public function markAsRead(MessageInterface $message);

    /**
     * @param MessageInterface $message
     */
    public function archive(MessageInterface $message);
}
