<?php

namespace App\Saver;

use App\Entity\MessageInterface;
use Doctrine\ORM\EntityManagerInterface;

class MessageSaver implements MessageSaverInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * MessageSaver constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function save(MessageInterface $message)
    {
        $this->em->persist($message);
        $this->em->flush();
    }
}
