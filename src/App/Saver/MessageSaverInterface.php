<?php

namespace App\Saver;

use App\Entity\MessageInterface;

interface MessageSaverInterface
{
    /**
     * @param MessageInterface $message
     */
    public function save(MessageInterface $message);
}
