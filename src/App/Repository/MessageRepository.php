<?php

namespace App\Repository;

use App\Entity\Message;
use App\Entity\MessageInterface;
use App\Exception\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class MessageRepository extends EntityRepository implements MessageRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return MessageInterface
     *
     * @throws EntityNotFoundException
     */
    public function findOneById(int $id): MessageInterface
    {
        $message = $this->find($id);

        if (null === $message) {
            throw new EntityNotFoundException();
        }

        return $message;
    }

    /**
     * @param int $from
     * @param int $limit
     *
     * @return array
     */
    public function get(int $from, int $limit): array
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('m')
            ->from(Message::class, 'm')
            ->setFirstResult($from)
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return int
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getCount(): int
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('COUNT(m)')
            ->from(Message::class, 'm');

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param int $from
     * @param int $limit
     *
     * @return array
     */
    public function getArchived(int $from, int $limit): array
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('m')
            ->from(Message::class, 'm')
            ->setFirstResult($from)
            ->where(
                $qb->expr()->eq('m.archived', $qb->expr()->literal(1))
            )
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return int
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getArchivedCount(): int
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('COUNT(m)')
            ->from(Message::class, 'm')
            ->where(
                $qb->expr()->eq('m.archived', $qb->expr()->literal(1))
            );

        return $qb->getQuery()->getSingleScalarResult();
    }
}
