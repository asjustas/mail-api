<?php

namespace App\Repository;

use App\Entity\MessageInterface;
use App\Exception\EntityNotFoundException;

interface MessageRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return MessageInterface
     *
     * @throws EntityNotFoundException
     */
    public function findOneById(int $id): MessageInterface;

    /**
     * @param int $from
     * @param int $limit
     *
     * @return array
     */
    public function get(int $from, int $limit): array;

    /**
     * @return int
     */
    public function getCount(): int;

    /**
     * @param int $from
     * @param int $perPage
     *
     * @return array
     */
    public function getArchived(int $from, int $perPage): array;

    /**
     * @return int
     */
    public function getArchivedCount(): int;
}
