<?php

namespace App\Controller;

use App\Exception\EntityNotFoundException;
use App\Manager\MessageManagerInterface;
use App\Repository\MessageRepositoryInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class MessageController
 * @package App\Controller
 */
class MessageController
{
    const PER_PAGE = 20;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MessageManagerInterface
     */
    private $manager;

    /**
     * @var MessageRepositoryInterface
     */
    private $repository;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * MessageController constructor.
     * @param LoggerInterface $logger
     * @param MessageManagerInterface $manager
     * @param MessageRepositoryInterface $repository
     * @param RequestStack $requestStack
     */
    public function __construct(
        LoggerInterface $logger,
        MessageManagerInterface $manager,
        MessageRepositoryInterface $repository,
        RequestStack $requestStack
    ) {
        $this->logger = $logger;
        $this->manager = $manager;
        $this->repository = $repository;
        $this->requestStack = $requestStack;
    }

    /**
     * @return JsonResponse
     */
    public function listAction()
    {
        try {
            $request = $this->requestStack->getCurrentRequest();

            if (is_null($request)) {
                throw new \Exception("Can't get current request");
            }

            $perPage = $request->get('per_page', self::PER_PAGE);
            $page = $request->get('page', 1);
            $from = ($page * $perPage) - $perPage;

            return new JsonResponse(
                $this->repository->get($from, $perPage),
                200,
                [
                    'X-Total-Count' => $this->repository->getCount(),
                ]
            );
        } catch (\Exception $e) {
            $this->logger->error($e, $e->getTrace());

            return new JsonResponse(null, 500);
        }
    }

    /**
     * @param int $messageId
     * @return JsonResponse
     */
    public function showAction(int $messageId)
    {
        try {
            return new JsonResponse(
                $this->repository->findOneById($messageId)
            );
        } catch (EntityNotFoundException $e) {
            return new JsonResponse(null, 404);
        } catch (Exception $e) {
            $this->logger->error($e, $e->getTrace());

            return new JsonResponse(null, 500);
        }
    }

    /**
     * @param int $messageId
     * @return JsonResponse
     */
    public function readAction(int $messageId)
    {
        try {
            $this
                ->manager
                ->markAsRead(
                    $this->repository->findOneById($messageId)
                );

            return new JsonResponse(null, 200);
        } catch (EntityNotFoundException $e) {
            return new JsonResponse(null, 404);
        } catch (Exception $e) {
            $this->logger->error($e, $e->getTrace());

            return new JsonResponse(null, 500);
        }
    }

    /**
     * @param int $messageId
     * @return JsonResponse
     */
    public function archiveAction(int $messageId)
    {
        try {
            $this
                ->manager
                ->archive(
                    $this->repository->findOneById($messageId)
                );

            return new JsonResponse(null, 200);
        } catch (EntityNotFoundException $e) {
            return new JsonResponse(null, 404);
        } catch (Exception $e) {
            $this->logger->error($e, $e->getTrace());

            return new JsonResponse(null, 500);
        }
    }
}
