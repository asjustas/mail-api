<?php

namespace App\Controller;

use App\Repository\MessageRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ArchivedMessageController
 * @package App\Controller
 */
class ArchivedMessageController
{
    const PER_PAGE = 20;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MessageRepositoryInterface
     */
    private $repository;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * ArchivedMessageController constructor.
     * @param LoggerInterface $logger
     * @param MessageRepositoryInterface $repository
     * @param RequestStack $requestStack
     */
    public function __construct(
        LoggerInterface $logger,
        MessageRepositoryInterface $repository,
        RequestStack $requestStack
    ) {
        $this->logger = $logger;
        $this->repository = $repository;
        $this->requestStack = $requestStack;
    }

    /**
     * @return JsonResponse
     */
    public function listAction()
    {
        try {
            $request = $this->requestStack->getCurrentRequest();

            if (is_null($request)) {
                throw new \Exception("Can't get current request");
            }

            $perPage = $request->get('per_page', self::PER_PAGE);
            $page = $request->get('page', 1);
            $from = ($page * $perPage) - $perPage;

            return new JsonResponse(
                $this->repository->getArchived($from, $perPage),
                200,
                [
                    'X-Total-Count' => $this->repository->getArchivedCount(),
                ]
            );
        } catch (\Exception $e) {
            $this->logger->error($e, $e->getTrace());

            return new JsonResponse($e->getMessage(), 500);
        }
    }
}
