<?php

namespace App\Command;

use App\Importer\MessagesImporterInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportMessagesCommand extends Command
{
    /**
     * @var MessagesImporterInterface
     */
    private $importer;

    /**
     * ImportMessagesCommand constructor.
     * @param MessagesImporterInterface $importer
     */
    public function __construct(MessagesImporterInterface $importer)
    {
        parent::__construct();

        $this->importer = $importer;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:import:messages')
            ->setDescription('Import messages from json file');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Starting import messages</info>');

        try {
            $this->importer->import(__DIR__ . '/messages_sample.json');
        } catch (Exception $e) {
            $output
                ->writeln(
                    sprintf(
                        '<error>Error importing messages: %s</error>',
                        $e->getMessage()
                    )
                );
        }
    }
}
