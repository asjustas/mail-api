<?php

namespace App\Importer;

interface MessagesImporterInterface
{
    /**
     * @param string $filename
     */
    public function import(string $filename);
}
