<?php

namespace App\Importer;

use App\Entity\Message;
use App\Saver\MessageSaverInterface;
use JsonStreamingParser\Parser;
use JsonStreamingParser\Listener\GeoJsonListener;

class MessagesImporter implements MessagesImporterInterface
{
    /**
     * @var MessageSaverInterface
     */
    private $saver;

    /**
     * MessagesImporter constructor.
     * @param MessageSaverInterface $saver
     */
    public function __construct(MessageSaverInterface $saver)
    {
        $this->saver = $saver;
    }

    /**
     * @param string $filename
     */
    public function import(string $filename)
    {
        $stream = fopen($filename, 'r');
        $listener = new GeoJsonListener(function (array $message) {
            $this->importMessage($message);
        });

        try {
            $parser = new Parser($stream, $listener);
            $parser->parse();
        } finally {
            fclose($stream);
        }
    }

    /**
     * @param array $message
     */
    private function importMessage(array $message)
    {
        $entity = new Message(
            $message['sender'],
            $message['subject'],
            $message['message'],
            $message['time_sent'],
            false,
            false
        );

        $this->saver->save($entity);
    }
}
