<?php

namespace App\Exception;

/**
 * Class EntityNotFoundException
 * @package App\Exception
 */
class EntityNotFoundException extends \Exception
{
}
