# Setup project

To run project you need following things:

- php 7.2
- mysql server
- composer

when you run `composer install` you will be asked to provide project parameters.
After that you need to run following commands:

- `bin/console doctrine:database:create`
- `bin/console doctrine:schema:create`
- `bin/console app:import:messages`

To start project you can use build in php server `cd web` and run `php -S 127.0.0.1:8000`

# Api documentation

Api docs are written using [swagger](https://swagger.io/) you can find them in `docs` directory.
Api is protected using `basic auth`

Username: `root`

Password: `root`

# Todo before running in production:

- implement normal authentication
- define time_sent as datetime and return it as datetime formatted string (not as int like right now)
- allow to load kernel with `prod` env and `debug` set to `false` (now `index.php` loads `dev` env)
- setup cache
- add `CORS` header if needed
- add parameter to `import messages` command so you could provide path to messages file
- refactor messages importer so `Parser` would be injected as dependency and we could test messages importer
- write more phpspec tests
- add behat and write some `behat` tests.
- refactor `MessagesController` and `ArchivedMessagesController` to remove duplicating code

# Why symfony

- used symfony because I wanted to not reinvent the wheel and save some time.
- used 3.4.1 version because of long term support https://symfony.com/roadmap?version=3.4#checker
- used MicroKernelTrait to save some resources
